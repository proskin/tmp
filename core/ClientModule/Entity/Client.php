<?php declare(strict_types=1);

namespace xxx\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Embedded;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\OneToMany;
use xxx\Entity\Embeddable\Agreement;
use xxx\Entity\Embeddable\Card;
use xxx\Entity\Embeddable\Login;
use xxx\Entity\Embeddable\Phone;
use xxx\Entity\Embeddable\Saving;
use xxx\Entity\Embeddable\Title;

/**
 * Class Client
 *
 * @Entity(repositoryClass="App\ClientModule\Repository\ClientRepository")
 *
 * @package App\Entity
 */
class Client implements IEntity
{

    use Identifier;
    use EntityHelperTrait;

    /**
     * @var int
     * @Column(type="integer", unique=true)
     */
    private $ceosId;

    /**
     * @var string
     * @Column(type="string")
     */
    private $ean;

    /**
     * @var \xxx\Entity\Embeddable\Title
     * @Embedded(class="\xxx\Entity\Embeddable\Title")
     */
    private $title;

    /**
     * @var string
     * @Column(type="string")
     */
    private $firstName;

    /**
     * @var string
     * @Column(type="string")
     */
    private $lastName;

    /**
     * @var string
     * @Column(type="string")
     */
    private $email;

    /**
     * @var Phone
     * @Embedded(class="xxx\Entity\Embeddable\Phone")
     */
    private $phone;

    /**
     * @var string
     * @Column(type="string", length=1)
     */
    private $sex;

    /**
     * @var \DateTimeInterface
     * @Column(type="datetime")
     */
    private $birth;

    /**
     * @var string
     * @Column(type="string", length=1)
     */
    private $identityState;

    /**
     * @var \xxx\Entity\Embeddable\Card
     * @Embedded(class="\xxx\Entity\Embeddable\Card")
     */
    private $card;

    /**
     * @var \xxx\Entity\Embeddable\Saving
     * @Embedded(class="\xxx\Entity\Embeddable\Saving")
     */
    private $saving;

    /**
     * @var \DateTimeInterface
     * @Column(type="datetime")
     */
    private $registration;

    /**
     * @var Agreement
     * @Embedded(class="xxx\Entity\Embeddable\Agreement")
     */
    private $agreement;

    /**
     * @var \xxx\Entity\Embeddable\Login
     * @Embedded(class="xxx\Entity\Embeddable\Login")
     */
    private $login;

    /**
     * @var Address[]
     * @OneToMany(targetEntity="xxx\Entity\Address", mappedBy="client")
     */
    private $addresses = [];

    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->title = new Title();
        $this->agreement = new Agreement();
        $this->phone = new Phone();
        $this->saving = new Saving();
        $this->card = new Card();
    }

    /**
     * @var string
     * @TODO check passwordhash length
     * @Column(type="string", length=255)
     */
    private $password;

    /**
     * @return int
     */
    public function getCeosId(): int
    {
        return $this->ceosId;
    }

    /**
     * @param int $ceosId
     */
    public function setCeosId(int $ceosId)
    {
        $this->ceosId = $ceosId;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getBirth(): \DateTimeInterface
    {
        return $this->birth;
    }

    /**
     * @param \DateTimeInterface $birth
     */
    public function setBirth(\DateTimeInterface $birth)
    {
        $this->birth = $birth;
    }

    /**
     * @return Phone
     */
    public function getPhone(): Phone
    {
        return $this->phone;
    }

    /**
     * @param \xxx\Entity\Embeddable\Phone $phone
     */
    public function setPhone(Phone $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return \xxx\Entity\Embeddable\Agreement
     */
    public function getAgreement(): Agreement
    {
        return $this->agreement;
    }

    /**
     * @param \xxx\Entity\Embeddable\Agreement $agreement
     */
    public function setAgreement(Agreement $agreement)
    {
        $this->agreement = $agreement;
    }

    /**
     * @return \xxx\Entity\Embeddable\Title
     */
    public function getTitle(): \xxx\Entity\Embeddable\Title
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return \xxx\Entity\Address[]
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * @param \xxx\Entity\Address $address
     */
    public function addAddress(\xxx\Entity\Address $address)
    {
        $address->setClient($this);
        $this->addresses[] = $address;
    }

    /**
     * @return string
     */
    public function getSex(): string
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     * @throws \InvalidArgumentException
     */
    public function setSex(string $sex)
    {
        $sex = \strtoupper($sex);

        if (!\in_array($sex, ['F', 'M', 'N'], TRUE)) {
            throw new \InvalidArgumentException(
                \sprintf('Sex can only be M, F or N (`%s` supplied)', $sex)
            );
        }

        $this->sex = $sex;
    }

    /**
     * @return string
     */
    public function getEan(): string
    {
        return (string)$this->ean; //Return value of xxx\\Entity\\Client::getEan() must be of the type string, null returned
    }

    /**
     * @param string $ean
     */
    public function setEan(string $ean)
    {
        $this->ean = $ean;
    }

    /**
     * @return string
     */
    public function getIdentityState(): string
    {
        return $this->identityState;
    }

    /**
     * @param string $identityState
     */
    public function setIdentityState(string $identityState)
    {
        $this->identityState = $identityState;
    }

    /**
     * @return \xxx\Entity\Embeddable\Card
     */
    public function getCard(): \xxx\Entity\Embeddable\Card
    {
        return $this->card;
    }

    /**
     * @param \xxx\Entity\Embeddable\Card $card
     */
    public function setCard(\xxx\Entity\Embeddable\Card $card)
    {
        $this->card = $card;
    }

    /**
     * @return \xxx\Entity\Embeddable\Saving
     */
    public function getSaving(): \xxx\Entity\Embeddable\Saving
    {
        return $this->saving;
    }

    /**
     * @param \xxx\Entity\Embeddable\Saving $saving
     */
    public function setSaving(\xxx\Entity\Embeddable\Saving $saving)
    {
        $this->saving = $saving;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getRegistration(): \DateTimeInterface
    {
        return $this->registration;
    }

    /**
     * @param \DateTimeInterface $registration
     */
    public function setRegistration(\DateTimeInterface $registration)
    {
        $this->registration = $registration;
    }

    /**
     * @return \xxx\Entity\Embeddable\Login
     */
    public function getLogin(): \xxx\Entity\Embeddable\Login
    {
        return $this->login;
    }

    /**
     * @param \xxx\Entity\Embeddable\Login $login
     */
    public function setLogin(\xxx\Entity\Embeddable\Login $login)
    {
        $this->login = $login;
    }

    /**
     * @param array $sourceArray
     * @return \xxx\Entity\Client
     */
    public function update(array $sourceArray = []): self
    {

        [
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'email' => $this->email,
            'ean' => $this->ean,
            //'identityState' => $this->identityState,
            'ceosId' => $this->ceosId
        ] = $sourceArray;

        /**
         * This is because registration and login response is a bit different
         */

        $this->password = $sourceArray['password'] ?? NULL;

        $this->birth = new \DateTime($sourceArray['birth']);

        if(!isset($sourceArray['registration'])) {
            $sourceArray['registration'] = date('Y-m-d');
        }

        $this->registration = new \DateTime($sourceArray['registration']);

        $this->setSex($sourceArray['sex']);

        $this->title = Title::fromArray($sourceArray['title']);
        $this->phone = Phone::fromArray($sourceArray['phone']);
        $this->agreement = Agreement::fromArray($sourceArray['agreement']);
        if(isset($sourceArray['saving'])) {
            $this->saving = Saving::fromArray($sourceArray['saving']);
        }
        if(isset($sourceArray['card'])) {
            $this->card = Card::fromArray($sourceArray['card']);
        }


        $this->login = new Login();
        //$this->login->setLastSuccess(new \DateTime($sourceArray['loginLastSuccess']));
        //$this->login->setBlockedFrom(new \DateTime($sourceArray['loginBlockedFrom']));
        //$this->login->setErrorDate(new \DateTime($sourceArray['loginErrorDate']));

        foreach ($sourceArray['addresses'] ?? [] as $address) {
            $this->addAddress(Address::fromArray($address));
        }

        $this->id = $this->ceosId;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {

        return [
            'ceosId' => $this->getCeosId(),
            'ean' => $this->getEan(),
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'title' => $this->getTitle()->toArray(),
            'email' => $this->getEmail(),
            'sex' => $this->getSex(),
            'birth' => $this->getBirth()->format(\DateTime::ATOM),
            'phone' => $this->getPhone()->toArray(),
            'agreement' => $this->getAgreement()->toArray(),
            'saving' => $this->getSaving()->toArray(),
            'registration' => $this->getRegistration()->format(\DateTime::ATOM),
            'card' => $this->getCard()->toArray(),

            'addresses' => \array_map(function (Address $address) {
                return $address->toArray();
            }, $this->getAddresses()),
        ];
    }

}
