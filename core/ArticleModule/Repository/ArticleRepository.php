<?php declare(strict_types=1);

namespace App\ArticleModule\Repository;

use App\CoreModule\Exception\NotFoundException;
use App\CoreModule\Exception\UnrecoverableException;
use App\CoreModule\VO\Query\Limit;
use App\CoreModule\VO\Query\Order;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;
use xxx\Entity\Article;
use xxx\VO\Uid;

class ArticleRepository extends EntityRepository
{

	/**
	 * @param \xxx\VO\Uid $uid
	 * @return \xxx\Entity\Article
	 * @throws \App\CoreModule\Exception\NotFoundException
	 */
	public function findById(Uid $uid): Article
	{
		/** @var Article|null $article */
		$article = $this->find($uid->get());

		if ($article === NULL) {
			throw NotFoundException::byId(Article::class, $uid);
		}

		return $article;
	}

	/**
	 * @param \App\CoreModule\VO\Query\Order $order
	 * @param \App\CoreModule\VO\Query\Limit $limit
	 * @return Article[]
	 */
	public function findAllSorted(Order $order, Limit $limit): array
	{
		$queryBuilder = $this->createQueryBuilder('n')
			->orderBy('n.' . $order->getField(), $order->getOrder());

		if ($limit->isLimited()) {
			$queryBuilder->setMaxResults($limit->get());
		}

		return $queryBuilder
			->getQuery()
			->getResult();
	}

	/**
	 * @param Article $article
	 * @return Article
	 * @throws \App\CoreModule\Exception\UnrecoverableException
	 */
	public function save(Article $article): Article
	{
		$entityManager = $this->getEntityManager();

		try {

			$entityManager->persist($article);
			$entityManager->flush();

			return $article;
		} catch (OptimisticLockException | ORMInvalidArgumentException $exception) {
			throw UnrecoverableException::fromSaving(Article::class, $exception);
		}
	}

	/**
	 * @param Article $article
	 * @return bool
	 * @throws \App\CoreModule\Exception\UnrecoverableException
	 */
	public function delete(Article $article): bool
	{
		$entityManager = $this->getEntityManager();

		try {

			$entityManager->remove($article);
			$entityManager->flush();

			return TRUE;
		} catch (OptimisticLockException | ORMInvalidArgumentException $exception) {
			throw UnrecoverableException::fromRemoving(Article::class, $exception);
		}
	}

}
