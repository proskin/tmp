<?php declare(strict_types=1);

namespace App\ArticleModule\Controller;

use Apitte\Core\Annotation\Controller\Controller;
use Apitte\Core\Annotation\Controller\ControllerPath;
use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Annotation\Controller\RequestParameter;
use Apitte\Core\Annotation\Controller\RequestParameters;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use App\ArticleModule\Service\ArticleService;
use App\CoreModule\Controller\BaseV1Controller;
use App\CoreModule\VO\Query\Limit;
use App\CoreModule\VO\Query\Order;
use xxx\Entity\Article;
use Nette\Http\IResponse;

/**
 * @Controller()
 * @ControllerPath("/article")
 */
final class ArticleController extends BaseV1Controller
{

	/**
	 * @var \App\ArticleModule\Service\ArticleService
	 */
	private $articleService;

	/**
	 * @param \App\ArticleModule\Service\ArticleService $articleService
	 */
	public function __construct(ArticleService $articleService)
	{
		$this->articleService = $articleService;
	}

	/**
	 * @Method("GET")
	 * @Path("/")
	 *
	 * @RequestParameters({
	 *     @RequestParameter(name="limit", type="int", in="query"),
	 *     @RequestParameter(name="sort", type="string", in="query")
	 * })
	 *
	 * @param \Apitte\Core\Http\ApiRequest $request
	 * @param \Apitte\Core\Http\ApiResponse $response
	 * @return \Apitte\Core\Http\ApiResponse
	 */
	public function list(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		$limit = new Limit($request->getParameter('limit', Limit::NO_LIMIT));
		$sort = $request->getQueryParam('sort', Order::DEFAULT_FIELD);

		$order = Order::createFromQuery($sort, Article::class);

		$articles = $this->articleService->getAllAsArray($limit, $order);

		return $response->writeJsonBody($articles);
	}

	/**
	 * @Method("POST")
	 * @Path("/")
	 *
	 * @param \Apitte\Core\Http\ApiRequest $request
	 * @param \Apitte\Core\Http\ApiResponse $response
	 * @return \Apitte\Core\Http\ApiResponse
	 */
	public function create(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		$article = $this->articleService->createFromRequest($request);

		return $response
			->writeJsonBody($article->toArray())
			->withStatus(IResponse::S201_CREATED);
	}

	/**
	 * @Method("GET")
	 * @Path("/{id}")
	 *
	 * @RequestParameters({
	 *     @RequestParameter(name="id", type="uid")
	 * })
	 *
	 * @param \Apitte\Core\Http\ApiRequest $request
	 * @param \Apitte\Core\Http\ApiResponse $response
	 * @return \Apitte\Core\Http\ApiResponse
	 */
	public function get(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		$id = $request->getParameter('id');

		$article = $this->articleService->get($id);

		return $response->writeJsonBody($article->toArray());
	}

	/**
	 * @Method("PUT")
	 * @Path("/{id}")
	 *
	 * @RequestParameters({
	 *     @RequestParameter(name="id", type="uid")
	 * })
	 *
	 * @param \Apitte\Core\Http\ApiRequest $request
	 * @param \Apitte\Core\Http\ApiResponse $response
	 * @return \Apitte\Core\Http\ApiResponse
	 */
	public function update(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		$id = $request->getParameter('id');

		$article = $this->articleService->updateFromRequest($id, $request);

		return $response->writeJsonBody($article->toArray());
	}

	/**
	 * @Method("DELETE")
	 * @Path("/{id}")
	 *
	 * @RequestParameters({
	 *     @RequestParameter(name="id", type="uid")
	 * })
	 *
	 * @param \Apitte\Core\Http\ApiRequest $request
	 * @param \Apitte\Core\Http\ApiResponse $response
	 * @return \Apitte\Core\Http\ApiResponse
	 */
	public function delete(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		$id = $request->getParameter('id');

		$this->articleService->delete($id);

		return $response->writeJsonBody(['message' => 'Deleted']);
	}

	/**
	 * @Method("PUT")
	 * @Path("/{id}/publish")
	 *
	 * @RequestParameters({
	 *     @RequestParameter(name="id", type="uid")
	 * })
	 *
	 * @param \Apitte\Core\Http\ApiRequest $request
	 * @param \Apitte\Core\Http\ApiResponse $response
	 * @return \Apitte\Core\Http\ApiResponse
	 */
	public function publish(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		$id = $request->getParameter('id');

		$article = $this->articleService->publish($id);

		return $response->writeJsonBody($article->toArray());
	}

	/**
	 * @Method("PUT")
	 * @Path("/{id}/retract")
	 *
	 * @RequestParameters({
	 *     @RequestParameter(name="id", type="uid")
	 * })
	 *
	 * @param \Apitte\Core\Http\ApiRequest $request
	 * @param \Apitte\Core\Http\ApiResponse $response
	 * @return \Apitte\Core\Http\ApiResponse
	 */
	public function retract(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		$id = $request->getParameter('id');

		$article = $this->articleService->retract($id);

		return $response->writeJsonBody($article->toArray());
	}

}
