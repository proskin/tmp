<?php declare(strict_types=1);

namespace xxx\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;

/**
 * Class DiseaseArticle
 *
 * @Entity(repositoryClass="App\ArticleModule\Repository\ArticleRepository")
 */
class Article implements IEntity
{

	use Identifier;
	use EntityHelperTrait;

	public const STATE_ACTIVE = 1;

	public const STATE_NOT_ACTIVE = 2;

	/**
	 * @var \DateTimeInterface|null
	 * @Column(type="datetime", nullable=true)
	 */
	private $publishDate;

	/**
	 * @var string
	 * @Column(type="string", length=255)
	 */
	private $title;

	/**
	 * @var string
	 * @Column(type="string", length=255)
	 */
	private $perex;

	/**
	 * @var string
	 * @Column(type="text")
	 */
	private $content;

	/**
	 * @var int
	 * @Column(type="smallint")
	 */
	private $state = self::STATE_NOT_ACTIVE;

	/**
	 * @var string
	 * @Column(type="string", length=255)
	 */
	private $imagePath;

	/**
	 * @param string $title
	 * @param string $perex
	 * @param string $content
	 */
	public function compose(string $title, string $perex, string $content)
	{
		$this->title = $title;
		$this->perex = $perex;
		$this->content = $content;
	}

	/**
	 * @return int
	 */
	public function getState(): int
	{
		return $this->state;
	}

	public function publish(): void
	{
		$this->state = self::STATE_ACTIVE;
		$this->publishDate = new \DateTimeImmutable();
	}

	public function retract(): void
	{
		$this->state = self::STATE_NOT_ACTIVE;
		$this->publishDate = NULL;
	}

	/**
	 * @param mixed $state
	 */
	public function setState($state)
	{
		$this->state = $state;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getPerex(): string
	{
		return $this->perex;
	}

	/**
	 * @return string
	 */
	public function getContent(): string
	{
		return $this->content;
	}

	/**
	 * @return string
	 */
	public function getImagePath(): string
	{
		return $this->imagePath;
	}

	/**
	 * @param string $imagePath
	 */
	public function setImagePath(string $imagePath): void
	{
		$this->imagePath = $imagePath;
	}

	/**
	 * @param \DateTimeInterface $publishDate
	 */
	public function setPublishDate(\DateTimeInterface $publishDate)
	{
		$this->publishDate = clone $publishDate;
	}

	/**
	 * @return \DateTimeInterface
	 */
	public function getPublishDate(): ?\DateTimeInterface
	{
		return $this->publishDate;
	}

	/**
	 * @return array
	 */
	public function toArray(): array
	{
		$date = $this->getPublishDate();

		if ($date !== NULL) {
			$date = $date->format(\DateTime::ATOM);
		}

		return [
			'id' => $this->getId(),
			'title' => $this->getTitle(),
			'perex' => $this->getPerex(),
			'content' => $this->getContent(),
			'imagePath' => $this->getImagePath(),
			'state' => $this->getState(),
			'publishDate' => $date,
		];
	}

	/**
	 * @param array $source
	 * @return \xxx\Entity\Article
	 */
	public static function fromArray(array $source): Article
	{
		$article = new self();

		$article->fromArrayFull($source);

		return $article;
	}

	/**
	 * @internal
	 * @param array $source
	 * @return \xxx\Entity\Article
	 */
	public function fromArrayFull(array $source): self
	{
		[
			'state' => $this->state,
			'imagePath' => $this->imagePath,
		] = $source;

		$this->compose($source['title'], $source['perex'], $source['content']);

		if (isset($source['id'])) {
			$this->id = $source['id'];
		}

		$this->publishDate = $source['publishDate'] ? new \DateTime($source['publishDate']) : NULL;

		return $this;
	}

	/**
	 * @param array $source
	 * @return \xxx\Entity\Article
	 */
	public function update(array $source): self
	{
		return $this->fromArrayFull($source);
	}

}
