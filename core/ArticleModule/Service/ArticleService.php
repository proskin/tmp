<?php declare(strict_types=1);

namespace App\ArticleModule\Service;

use Apitte\Core\Http\ApiRequest;
use App\ArticleModule\Repository\ArticleRepository;
use App\ArticleModule\Request\IArticleCreateRequest;
use App\CoreModule\Utils\RequestValidator;
use App\CoreModule\VO\Query\Limit;
use App\CoreModule\VO\Query\Order;
use xxx\Entity\Article;
use xxx\VO\Uid;

class ArticleService
{

	/**
	 * @var ArticleRepository
	 */
	private $articleRepository;

	/**
	 * @param \App\ArticleModule\Repository\ArticleRepository $articleRepository
	 */
	public function __construct(ArticleRepository $articleRepository)
	{
		$this->articleRepository = $articleRepository;
	}

	/**
	 * @param ApiRequest $request
	 * @return \xxx\Entity\Article
	 * @throws \App\CoreModule\Exception\UnrecoverableException
	 * @throws \App\CoreModule\Exception\InvalidRequestBodyException
	 */
	public function createFromRequest(ApiRequest $request): Article
	{
		$requestBody = $request->getJsonBody();

		RequestValidator::validate(IArticleCreateRequest::FIELDS, $requestBody);

		$article = Article::fromArray($requestBody);

		$this->articleRepository->save($article);

		return $article;
	}

	/**
	 * @param \App\CoreModule\VO\Query\Limit $limit
	 * @param \App\CoreModule\VO\Query\Order $order
	 * @return Article[]
	 */
	public function getAll(Limit $limit, Order $order): array
	{
		return $this->articleRepository->findAllSorted($order, $limit);
	}

	/**
	 * @param \App\CoreModule\VO\Query\Limit $limit
	 * @param \App\CoreModule\VO\Query\Order $order
	 * @return array
	 */
	public function getAllAsArray(Limit $limit, Order $order): array
	{
		$articles = $this->getAll($limit, $order);

		return \array_map(function (Article $article) {
			return $article->toArray();
		}, $articles);
	}

	/**
	 * @param \xxx\VO\Uid $uid
	 * @return \xxx\Entity\Article
	 * @throws \App\CoreModule\Exception\NotFoundException
	 */
	public function get(Uid $uid): Article
	{
		return $this->articleRepository->findById($uid);
	}

	/**
	 * @param \xxx\VO\Uid $uid
	 * @param \Apitte\Core\Http\ApiRequest $request
	 * @return \xxx\Entity\Article
	 * @throws \App\CoreModule\Exception\NotFoundException
	 * @throws \App\CoreModule\Exception\UnrecoverableException
	 * @throws \App\CoreModule\Exception\InvalidRequestBodyException
	 */
	public function updateFromRequest(Uid $uid, ApiRequest $request): Article
	{
		$article = $this->get($uid);

		$array = $request->getJsonBody();

		RequestValidator::validate(IArticleCreateRequest::FIELDS, $array);

		$article->update($array);

		$this->articleRepository->save($article);

		return $article;
	}

	/**
	 * @param \xxx\VO\Uid $uid
	 * @return bool
	 * @throws \App\CoreModule\Exception\UnrecoverableException
	 * @throws \App\CoreModule\Exception\NotFoundException
	 */
	public function delete(Uid $uid): bool
	{
		$article = $this->get($uid);

		$this->articleRepository->delete($article);

		return TRUE;
	}

	/**
	 * @param \xxx\VO\Uid $uid
	 * @return \xxx\Entity\Article
	 * @throws \App\CoreModule\Exception\UnrecoverableException
	 * @throws \App\CoreModule\Exception\NotFoundException
	 */
	public function publish(Uid $uid): Article
	{
		$article = $this->get($uid);

		$article->publish();

		$article = $this->articleRepository->save($article);

		return $article;
	}

	/**
	 * @param \xxx\VO\Uid $uid
	 * @return \xxx\Entity\Article
	 * @throws \App\CoreModule\Exception\UnrecoverableException
	 * @throws \App\CoreModule\Exception\NotFoundException
	 */
	public function retract(Uid $uid): Article
	{
		$article = $this->get($uid);

		$article->retract();

		$article = $this->articleRepository->save($article);

		return $article;
	}

}
