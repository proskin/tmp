<?php declare(strict_types=1);

namespace App\ArticleModule\Request;

interface IArticleCreateRequest
{

	public const FIELDS = [
		'title',
		'perex',
		'content',
		'state',
		'imagePath',
		'publishDate',
	];

}
