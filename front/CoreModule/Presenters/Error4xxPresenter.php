<?php declare(strict_types=1);

namespace App\CoreModule\Presenters;

use Nette;

class Error4xxPresenter extends BasePresenter
{

	public function startup()
	{
		parent::startup();
		if (!$this->getRequest()->isMethod(Nette\Application\Request::FORWARD)) {
			$this->error();
		}
	}

	public function renderDefault(Nette\Application\BadRequestException $exception)
	{
		$ds = DIRECTORY_SEPARATOR;
		$file = \realpath(__DIR__ . "{$ds}..{$ds}templates{$ds}Error{$ds}") . $ds . $exception->getCode() . '.latte';
		$this->template->setFile(\is_file($file)
			? $file : \realpath(__DIR__ . "{$ds}..{$ds}templates{$ds}Error") . $ds . '4xx.latte');
	}

}
