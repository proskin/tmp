<?php declare(strict_types=1);

namespace App\CoreModule\Presenters;

use	App\CoreModule\Service\BreadcrumbsService;
use Nette\Application\UI as UI;
use Nette\Utils\Strings;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends UI\Presenter
{
	/**
	* @var xxx/Entity/Client
	*/
	protected $client = null;

	protected $onlyForLoggedIn = false;

	/**
	 * @var \App\CoreModule\Service\BreadcrumbsService
	 */
	protected $bradcrumbsService;

	public function __construct(BreadcrumbsService $bradcrumbsService)
	{
		$this->bradcrumbsService = $bradcrumbsService;
	}

	public function startup()
	{
		$this->template->bodyClass = Strings::webalize($this->getName());

		if($this->getUser()->getIdentity() !== null) {
			$this->client = $this->getUser()->getIdentity()->getData()['client'];
		}

		if($this->onlyForLoggedIn === true && $this->getUser()->isLoggedIn() === false) {
			$this->flashMessage('Tato stránka je pouze pro přihlášené uživatele.');
			//@TODO save backling and after login, redirect on ('this') page
			$this->redirect(':Homepage:Homepage:default');
		}

		$this->bradcrumbsService->addCrumb('Domů', $this->link(':Homepage:Homepage:default'));

		parent::startup();
	}

	public function beforeRender()
	{
		$this->template->breadcrumbs = $this->bradcrumbsService->prepare();

		parent::beforeRender();
	}
}
