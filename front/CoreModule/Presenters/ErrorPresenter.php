<?php declare(strict_types=1);

namespace App\CoreModule\Presenters;

use Nette;
use	Nette\Application as Application;
use	Tracy\ILogger;

class ErrorPresenter implements Application\IPresenter
{

	use Nette\SmartObject;

	/** @var ILogger */
	private $logger;

	public function __construct(ILogger $logger)
	{
		$this->logger = $logger;
	}

	/**
	 * @param Application\Request $request
	 * @return Application\IResponse
	 */
	public function run(Application\Request $request)
	{
		$e = $request->getParameter('exception');

		if ($e instanceof Nette\Application\BadRequestException) {
			// $this->logger->log("HTTP code {$e->getCode()}: {$e->getMessage()} in {$e->getFile()}:{$e->getLine()}", 'access');
			list($module, , $sep) = Nette\Application\Helpers::splitName($request->getPresenterName());

			return new Application\Responses\ForwardResponse($request->setPresenterName($module . $sep . 'Error4xx'));
		}

		$this->logger->log($e, ILogger::EXCEPTION);

		return new Application\Responses\CallbackResponse(function () {
			$ds = DIRECTORY_SEPARATOR;
			/** @noinspection PhpIncludeInspection */
			require \realpath(__DIR__ . "{$ds}..{$ds}templates{$ds}Error") . $ds . '500.phtml';
		});
	}

}
