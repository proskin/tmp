<?php declare(strict_types=1);

namespace App\CoreModule\Service;

use App\CoreModule\Entity\Crumb;

class BreadcrumbsService
{

	private $bread = [];

	public function addCrumb(string $title, string $url = '')
	{
		$this->bread[] = new Crumb($title, $url);
	}

	/**
	 * @return \App\CoreModule\Entity\Crumb[]
	 */
	public function get()
	{
		return $this->bread;
	}

	/**
	 * @return array
	 */
	public function prepare()
	{
		/*$i = 0;
		$count = count($this->bread);
		foreach ($this->bread as $crumb) {

			if($count == $i) {
				$crumb->removeUrl();
			}

		}*/

		return $this->bread;
	}
}
