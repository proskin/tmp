<?php declare(strict_types=1);

namespace App\CoreModule\Service;


class RuianService
{

	private $server = FALSE;

	/**
	 * RuianService constructor.
	 *
	 * @param $server
	 */
	public function __construct($server)
	{
		$this->server = $server;
	}

	/**
	 * @param $post
	 * @return bool|mixed
	 */
	public function ruian(array $post)
	{
		//@TODO call through api ?

		$post = json_decode(json_encode($post), FALSE);
		$json = json_encode($post);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $this->server);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, "5");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

		$data = curl_exec($ch);
		$curlErrNo = curl_errno($ch);

		curl_close($ch);
		if ($curlErrNo !== 0) {
			return FALSE;
		}

		return $data;
	}

}
