<?php declare(strict_types=1);

namespace App\CoreModule\Service;


use Apitte\Core\Http\ApiResponse;
use App\CoreModule\VO\HttpMethod;
use Contributte\Psr7\Psr7Response;
use xxx\VO\Url;
use GuzzleHttp\Client;

/**
 * Class ApiService
 *
 * @package CoreModule\Service
 */
class ApiService
{

	public const NO_TIMEOUT = 0;

	/**
	 * @var float
	 */
	private $timeout;

	private $apiUrl;

	public function __construct(array $config,  int $timeout = self::NO_TIMEOUT)
	{
		$this->setConfig($config);
		$this->timeout = $timeout;
	}

	/**
	 * @param string $path
	 * @return \Apitte\Core\Http\ApiResponse
	 * @throws \DomainException
	 * @throws \RuntimeException
	 */
	public function get(string $path): ApiResponse
	{
		$url = Url::compose($this->apiUrl, $path);

		return $this->call(new HttpMethod('GET'), $url, []);
	}

	/**
	 * @param string $path
	 * @param array $payload
	 * @return \Apitte\Core\Http\ApiResponse
	 * @throws \DomainException
	 * @throws \RuntimeException
	 */
	public function post(string $path, array $payload): ApiResponse
	{
		$url = Url::compose($this->apiUrl, $path);

		return $this->call(new HttpMethod('POST'), $url, $payload);
	}

	/**
	 * @param string $path
	 * @param array $payload
	 * @return \Apitte\Core\Http\ApiResponse
	 * @throws \DomainException
	 * @throws \RuntimeException
	 */
	public function put(string $path, array $payload): ApiResponse
	{
		$url = Url::compose($this->apiUrl, $path);

		return $this->call(new HttpMethod('PUT'), $url, $payload);
	}

	/**
	 * @param \xxx\VO\HttpMethod $method
	 * @param \xxx\VO\Url $url
	 * @param array $payload
	 * @return \Apitte\Core\Http\ApiResponse
	 * @throws \RuntimeException
	 */
	protected function call(HttpMethod $method, Url $url, array $payload): ApiResponse
	{
		$guzzleClient = $this->createClient();

		try {

			$options = [
				'json' => $payload,
				'timeout' => $this->timeout,
			];

			$response = $guzzleClient->request((string)$method, (string)$url, $options);

			return new ApiResponse(Psr7Response::of($response));

		} catch (ClientException | ServerException $clientException) {

			$response = $clientException->getResponse();
			$body = $response ? $response->getBody()->getContents() : '{}';

			return new ApiResponse(new Psr7Response($response ? $response->getStatusCode() : 500, [], $body));
		}
	}

	/**
	 * @return \GuzzleHttp\Client
	 */
	protected function createClient(): Client
	{
		return new Client();
	}

	/**
	 * @param array $config
	 * @throws \App\Service\InvalidConfigException
	 */
	private function setConfig(array $config) {

		$this->apiUrl = $this->getParamFromConfig($config, 'apiUrl');
	}

	/**
	 * check if param from config exist and has value
	 *
	 * @param array $config
	 * @param string $paramName
	 * @return mixed
	 * @throws \App\Service\InvalidApiConfigException
	 */
	public function getParamFromConfig(array $config, string $paramName)
	{
		if(!\array_key_exists($paramName, $config)) {
			throw new InvalidApiConfigException("Missing config for param '".$paramName."'.");
		}

		if(empty($config[$paramName])) {
			throw new InvalidApiConfigException("Value for param '".$paramName."' can not be empty.");
		}

		return $config[$paramName];

	}
}
