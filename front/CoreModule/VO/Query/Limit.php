<?php declare(strict_types=1);

namespace App\CoreModule\VO\Query;

/**
 * Class Limit
 *
 * @package App\CoreModule\VO\Query
 */
class Limit
{

	/***
	 * @var int
	 */
	private $limit;

	/**
	 * Limit constructor.
	 *
	 * @param $limit
	 * @throws \DomainException
	 */
	public function __construct($limit)
	{

		if (!\is_numeric($limit) || !\is_int((int)$limit)) {
			throw new \DomainException(\sprintf('Limit must be integer(%d supplied)', $limit), 400);
		}

		$this->limit = (int)$limit;
	}

	/**
	 * @return int
	 */
	public function get(): int
	{
		return $this->limit;
	}
}
