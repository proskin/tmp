<?php declare(strict_types=1);

namespace App\CoreModule\VO\Query;

/**
 * Class Order
 *
 * @package App\CoreModule\VO\Query
 */
class Order
{

	/**
	 * @var string
	 */
	private $field;

	/**
	 * @var string
	 */
	private $order;

	/**
	 * Order constructor.
	 *
	 * @param string $field
	 * @param string $order
	 * @throws \DomainException
	 */
	public function __construct(string $field, string $order)
	{

		if ($order !== 'ASC' && $order !== 'DESC') {
			// @TODO
			throw new \DomainException('@TODO');
		}

		$this->field = $field;
		$this->order = $order;
	}

	/**
	 * @return string
	 */
	public function getOrder(): string
	{
		return $this->order;
	}

	/**
	 * @return string
	 */
	public function getField(): string
	{
		return $this->field;
	}

}
