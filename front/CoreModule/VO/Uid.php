<?php declare(strict_types=1);

namespace App\CoreModule\VO;

/**
 * Class Uid
 *
 * @package App\CoreModule\VO\Query
 */
class Uid
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * Uid constructor.
	 *
	 * @param int $id
	 * @throws \DomainException
	 */
	public function __construct(int $id)
	{

		if (!\is_int($id) || $id < 1) {
			throw new \DomainException(
				\sprintf('Id must be integer greater than zero(%d provided)', $id),
				400
			);
		}

		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function extract(): int
	{
		return $this->id;
	}

}
