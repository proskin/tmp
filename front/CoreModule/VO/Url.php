<?php declare(strict_types=1);

namespace App\CoreModule\VO;

/**
 * Class Url
 *
 * @package App\CoreModule\VO
 */
class Url
{

	/**
	 * @var string
	 */
	private $url;

	/**
	 * Url constructor.
	 *
	 * @param string $url
	 * @param array $params
	 * @throws \DomainException
	 */
	public function __construct(string $url, ...$params)
	{

		$url = \sprintf($url, ...$params);

		if (\filter_var($url, \FILTER_VALIDATE_URL) === FALSE) {
			throw new \DomainException('Url is not valid');
		}

		$this->url = $url;
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return $this->url;
	}

}
