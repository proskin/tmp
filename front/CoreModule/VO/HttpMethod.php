<?php declare(strict_types=1);

namespace App\CoreModule\VO;

/**
 * Class HttpMethod
 *
 * @package CoreModule\VO
 */
class HttpMethod
{

	/**
	 * @var string
	 */
	private $method;

	public const ALLOWED = ['GET', 'POST', 'PUT', 'DELETE'];

	/**
	 * HttpMethod constructor.
	 *
	 * @param string $method
	 * @throws \InvalidArgumentException
	 */
	public function __construct(string $method)
	{

		/**
		 * To have consistent method names
		 */
		$method = \strtoupper($method);

		if (!\in_array($method, self::ALLOWED, TRUE)) {
			throw new \DomainException(
				\sprintf('Method can only be %s', \implode(', ', self::ALLOWED))
			);
		}

		$this->method = $method;
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return $this->method;
	}

}
