<?php declare(strict_types=1);

namespace App\CoreModule\Entity;

class Crumb {

	private $title;

	private $url;

	/**
	 * Crumb constructor.
	 *
	 * @param string $title
	 * @param string $url
	 */
	public function __construct(string $title, string $url = "")
	{
		$this->title = $title;
		$this->url = $url;
	}

	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return $this->url;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

}
