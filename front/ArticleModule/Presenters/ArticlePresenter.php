<?php declare(strict_types=1);

namespace App\ArticleModule\Presenters;

use App\ArticleModule\Service\ArticleService;
use App\CoreModule\Presenters\BasePresenter;


class ArticlePresenter extends BasePresenter
{

	/**
	 * @var \App\ArticleModule\Service\ArticleService
	 */
	private $articleService;

	public function __construct(ArticleService $articleService)
	{
		$this->articleService = $articleService;
	}

	public function actionDefault()
	{
		$this->template->title = "Novinky";

		$this->template->articles = $this->articleService->getArticleList();
	}

}
