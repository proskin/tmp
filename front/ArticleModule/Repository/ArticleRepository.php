<?php declare(strict_types=1);

namespace App\ArticleModule\Repository;

use App\CoreModule\Service\ApiService;
use xxx\Entity\Article;


class ArticleRepository {

	/**
	 * @var \App\CoreModule\Service\ApiService
	 */
	private $apiService;

	public function __construct(ApiService $apiService)
	{
		$this->apiService = $apiService;
	}

	/**
	 * @param int $id
	 * @return /xxx/Entity/Article[]
	 */
	public function getArticleList()
	{
		$response = $this->apiService->get(sprintf('v1/article'));

		$data = $response->getJsonBody();

		return $this->fetchFromJsonArray($data);

	}

	/**
	 * @param array $data
	 * @return /xxx/Entity/Article[]
	 */
	public function fetchFromJsonArray(array $data): array
	{
		$articles = [];

		foreach ($data as $articleData) {
			$article = new Article();
			$article->update((array)$articleData);
			$articles[] = $article;
		}

		return $articles;
	}

}
