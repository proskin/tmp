<?php declare(strict_types=1);

namespace App\ArticleModule\Service;

use App\ArticleModule\Repository\ArticleRepository;


/**
 * Class ArticleService
 *
 * @package App\ArticleModule\Service
 */
class ArticleService
{
	/**
	 * @var \App\ArticleModule\Repository\ArticleRepository
	 */
	private $articleRepository;

	public function __construct(ArticleRepository $articleRepository)
	{
		$this->articleRepository = $articleRepository;
	}

	/**
	 * @param int $id
	 * @return /xxx/Entity/Article[]
	 */
	public function getArticleList()
	{
		return $this->articleRepository->getArticleList();
	}
}
